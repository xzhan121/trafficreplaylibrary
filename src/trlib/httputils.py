from multidict import CIMultiDict

async def read_http_stream(reader):
    '''
        Written with reference from https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html
        Used to read from the reader stream supplied in asyncio utility functions, aka StreamReader
        Relies on CIMultiDict from the multidict pip package
        returns a tuple of (
                            start line: str, 
                            headers: CIMultiDict, 
                            body:str
                           )
        
    '''

    # print(reader)
    headers = CIMultiDict()
    start_line = None
    body = None

    start_line = await reader.readline()
    start_line = start_line.decode().strip()

    hdr_line = await reader.readline()
    hdr_line = hdr_line.decode()

    # terminates on the last /r/n
    while hdr_line.strip() != '':
        hdr, val = hdr_line.split(":", maxsplit=1)
        headers.add(hdr, val)

        hdr_line = await reader.readline()
        hdr_line = hdr_line.decode()

    cl = 0  # content-length
    # TODO: this needs to be expanded to handle transfer encoding and error handling
    if 'content-length' in headers:
        cl = int(headers['content-length'])
        body = ''

        # asyncio protocol throws on packets one at a time onto the reader's buffer
        # so we read in a loop to account for bodies that exceed 1 packet
        while len(body) != cl:
            bytes_to_read = cl - len(body)
            # print("still need {0} bytes".format(bytes_to_read))
            data = await reader.read(bytes_to_read)
            # print("just read {0} bytes".format(len(data.decode())))
            body += data.decode()
            # print("body is now {0}/{1} bytes".format(len(body), cl))
        # print(body, cl)

    # print("status ", start_line)
    # print("headers ", headers)
    # print("body length ", len(body))
    return (start_line, headers, body)
